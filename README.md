
# Setup
  - 安裝 pip3 ```sudo apt-get install python3-pip```
  - 安裝 docker ```curl -sSL https://get.docker.com/ | sudo sh```
  - 將使用者加入 docker 的群組，執行 docker 指令就可以不用 sudo ```sudo usermod -aG docker <USER>```
  - 啟用 docker ```sudo service docker start```
  - 回到家目錄並且 clone 本專案，```git clone https://gitlab.com/Tocknicsu1/nctu-vmproxy```
  - clone librenms docker 的社群專案，```git clone https://github.com/setiseta/docker-librenms.git```
  - 切換到本專案目錄底下
  - 安裝 python 相依套件 ```sudo pip3 install -r requirements.txt```
  - 設定 config.py，執行 ```cp config.py.sample```，然後填妥HOST, USER, PASSWORD 
  - 執行 ```python3 generator.py```，先執行 ```1``` ，接著離開執行 ```0```
  - 編輯 data.yaml ，隨意將其中一個 sync 改為 true
  - 執行 ```python3 generator.py```，執行 ```2``` ，接著離開執行 ```0```
  - 執行 ```docker-compose up -d``` ，會看到 snmp_proxy_networks 被建立起來的字樣
  - 切換到 docker-librenms 的目錄底下，並且將 nctu-vmproxy/librenms-patch/docker-compose.yml 複製到 docker-librenms 底下，這個 patch 是要將 librenms 的 docker ip address 與 snmp proxy 綁在同一個子網域底下
  - 啟動 librenms 的 container ```docker-compose up -d```
  - 啟動完後瀏覽網頁已觸發安裝，接著執行 ```docker logs -f --tail 50 librenms```，等待直到出現 ```listening for connections```(按```ctrl + c```離開)
  - 再次重整網頁會看到以下錯誤。
```
chown -R librenms:librenms /opt/librenms/logs
setfacl -R -m g::rwx /opt/librenms/logs
setfacl -d -m g::rwx /opt/librenms/logs
```
  - 進入到 container 裡修正錯誤，```docker exec -it librenms /bin/bash```，並執行以上指令
  - ```exit``` 離開 container 後，在 docker-librenms 執行 ```docker-compose restart```
  - 檢視網頁是否有成功啟動
  - 登入後 ```右上角齒輪``` -> ```API``` -> ```API Settings```，新增一個 token，並且將 token 以及 librenms 的網址後填入 ```config.py```
  - 執行 ```python3 generator.py```，執行 ```3```，可以看到先前 sync 改為 true 的 device 被加入了
  - 若要新增同步的 devices 可以先進入 nctu-vmproxy 修改 data.yaml，然後執行 ```python3 generator``` 產生 dockerfile 以及和 librenms 同步，離開後再執行 docker-compose up -d 即可。

# 檔案架構
```
 |- config.py
 |- generater.py
 |- snmp
  |- config.py
  |- main.py 此檔案為 snmp agent
  |- mibs
   |- __init__.py 此檔案會抓取此資料夾底下所有 xxx_x_x..._x.py 的檔案當作 mib，並且以第一個 _ 後面的數字作為他的 oid
   |- base.py 讓其他 mibs 繼承用
   |- data_fetcher.py 會從 vm/vm_vm.py 查詢資料，並且建立 cache
   |- 其他 xxx_x_x_..._x.py
  |- vm
   |- vm_vm.py 查詢單一個體 vm 並且整理所有需要資料
   |- vm_host.py 主要用來查詢所有 vm 
   |- pchelper.py 輔助工具
 |- librenms-patch
  |- docker-compose.yml 替代 docker-librenms 中的 docker-compose.yml，主要用於設定網路。
```

# 參考資料
 - vmware doc: https://www.vmware.com/support/developer/vc-sdk/visdk25pubs/ReferenceGuide/left-pane.html  
    - 常用 performance: https://www.vmware.com/support/developer/vc-sdk/visdk25pubs/ReferenceGuide/vim.PerformanceManager.html
    - 常用 vm basic: https://www.vmware.com/support/developer/vc-sdk/visdk25pubs/ReferenceGuide/vim.VirtualMachine.html
 - mibs 查詢網站: http://cric.grenoble.cnrs.fr/Administrateurs/Outils/MIBS
 - pysnmp(較少需要使用): http://snmplabs.com/pysnmp/
 - pyVmomi: https://github.com/vmware/pyvmomi/tree/master/pyVmomi
    - 有時候需要直接進 ServerObjects.py 搜尋要的東西的位置
 
