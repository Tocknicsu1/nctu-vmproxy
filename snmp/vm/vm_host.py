#!/usr/bin/env python
# https://github.com/flakshack/SysAdminBoard
import atexit
import datetime
import json
import logging.config
import sys
import time

import pyVmomi
from pyVim import connect

from snmp.vm.pchelper import collect_properties, get_container_view

logger = logging.getLogger(__name__)


class Monitor:
    def __init__(self, host=None, user=None, pwd=None):
        # config need contain
        # config['host']: vcenter ip or domain
        # config['user']: user to login
        # config['pwd']: password for user
        self.config = {
            'host': host,
            'user': user,
            'pwd': pwd
        }
        self.server = None

    def connect(self):
        if self.server is not None:
            return
        try:
            logger.debug("Connecting to: " + self.config['host'])
            self.server = connect.SmartConnectNoSSL(
                host=self.config['host'],
                user=self.config['user'],
                pwd=self.config['pwd'],
            )
            # Turn the connection timeout off (default 900 sec)
            self.server._stub.connectionPoolTimeout = -1
        except Exception as error:
            logger.error("Error connecting to " +
                         self.config['host'] + str(error))
        atexit.register(connect.Disconnect, self.server)

    def fetch_data(self):
        if self.server is None:
            self.connect()
        container_view = get_container_view(
            self.server, obj_type=[pyVmomi.vim.VirtualMachine])
        query = [
            "name",
            "config.uuid",
            "summary.config.instanceUuid"
        ]
        props = collect_properties(
            self.server,
            container_view,
            pyVmomi.vim.VirtualMachine,
            query,
            include_mors=True)
        return props


if __name__ == '__main__':
    from snmp import config
    monitor = Monitor(
        host=config.HOST,
        user=config.USER,
        pwd=config.PASSWORD,
    )
    data = monitor.fetch_data()
