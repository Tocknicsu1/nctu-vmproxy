#!/usr/bin/env python
# https://github.com/flakshack/SysAdminBoard
import atexit
import datetime

import pyVmomi
from pyVim import connect


class Monitor:
    def __init__(self, host=None, user=None, pwd=None, uuid=None):
        # config need contain
        # config['host']: vcenter ip or domain
        # config['user']: user to login
        # config['pwd']: password for user
        # config['uuid']: monitor vm uuid,
        #                 getting this by summary.config.instanceUuid
        self.config = {
            'host': host,
            'user': user,
            'pwd': pwd,
            'uuid': uuid
        }
        self.server = None
        self.vm_instance = None

    def connect(self):
        """
        It will connect to vmware server
        """
        if self.server is not None:
            return
        try:
            print('Connecting to: %s' % (self.config['host']))
            self.server = connect.SmartConnectNoSSL(
                host=self.config['host'],
                user=self.config['user'],
                pwd=self.config['pwd']
            )
            # Turn the connection timeout off (default 900 sec)
            self.server._stub.connectionPoolTimeout = -1
        except Exception as error:
            print("Error connecting to " + self.config['host'] + str(error))
        search_index = self.server.content.searchIndex
        self.vm_instance = search_index.FindByUuid(
            None, self.config['uuid'], True, True)
        atexit.register(connect.Disconnect, self.server)

    def fetch_performance(self, counterIdList):
        manager = self.server.content.perfManager
        queryspec = pyVmomi.vim.PerformanceManager.QuerySpec()

        queryspec.entity = self.vm_instance

        metricList = []
        for counterId in counterIdList:
            metricList.append(pyVmomi.vim.PerformanceManager.MetricId(
                counterId=counterId
            ))

        queryspec.metricId = metricList
        queryspec.maxSample = 1
        queryspec.intervalId = 300
        queryspec.format = "normal"

        return manager.QueryPerf([queryspec])[0].value

    def get_metrice_last_data(self, data):
        try:
            if data.value[-1] == -1:
                return data.value[-2]
            return data.value[-1]
        except:
            return -1

    def fetch_all_metric(self):
        manager = self.server.content.perfManager
        manager_summary = manager.QueryPerfProviderSummary(self.vm_instance)
        manager_metric = manager.QueryAvailablePerfMetric(self.vm_instance)
        metric_list = [x.counterId for x in manager_metric]
        metric_info_list = manager.QueryPerfCounter(metric_list)
        return metric_info_list

    def _fetch_info_uptime(self):
        return self.vm_instance.summary.quickStats.uptimeSeconds * 100

    def _fetch_info_name(self):
        now = self.vm_instance
        full_name = now.name
        now = now.parent
        while now is not None:
            if now.name != 'vm':
                full_name = now.name + '/' + full_name
            now = now.parent
        return full_name.encode('utf8').decode('iso-8859-1').encode('iso-8859-1')

    def fetch_data(self):
        if self.server is None:
            self.connect()
        data = {}
        # setting info
        data['info'] = {}
        # setting info uptime
        data['info']['instance_uuid'] = self.config['uuid']
        data['info']['uptime'] = self._fetch_info_uptime()
        data['info']['name'] = self._fetch_info_name()
        # setting performance
        data['performance'] = {}
        # setting performance - cpu
        performance = self.fetch_performance([2, 24])
        try:
            data['performance']['cpu'] = self.get_metrice_last_data(performance[0]) // 100
        except:
            data['performance']['cpu'] = -1
        # setting performance - memory
        data['performance']['memory'] = {}
        total_memory = self.vm_instance.summary.quickStats.privateMemory
        try:
            usage_memory = total_memory * self.get_metrice_last_data(performance[1]) // 10000
        except:
            usage_memory = -1
        data['performance']['memory']['total'] = total_memory
        data['performance']['memory']['usage'] = usage_memory
        # setting performance - disk
        data['performance']['disk'] = {}
        data['performance']['disk']['total'] = 0
        data['performance']['disk']['usage'] = 0
        for disk in self.vm_instance.guest.disk:
            data['performance']['disk']['total'] += disk.capacity
            data['performance']['disk']['usage'] += disk.capacity - disk.freeSpace
        # self.vm_instance.config.hardware.memoryMB
        # print(self.vm_instance.summary.quickStats)
        # print('24', performance[1].value[-1])
        # print('427', performance[2].value[-1])
        # print('428', performance[3].value[-1])
        # print('280', performance[4])

        return data


if __name__ == '__main__':
    from snmp import config
    monitor = Monitor(
        host=config.HOST,
        user=config.USER,
        pwd=config.PASSWORD,
        uuid=config.VM_INSTANCE_UUID
    )
    monitor.connect()
    # data = monitor.fetch_data()
    metric_info_list = monitor.fetch_all_metric()
    for metric in metric_info_list:
        print(metric.key, metric.nameInfo.summary, metric.nameInfo.key)
        # if metric.key == 280:
        #     print(metric)
        #     print(metric.key, metric.nameInfo.summary, metric.nameInfo.key)
