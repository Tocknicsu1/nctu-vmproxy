from pysnmp.proto import api
from snmp.mibs.base import Base


class memBuffer(Base):

    def resolve(self, protoVer):
        return api.protoModules[protoVer].Integer(0)
