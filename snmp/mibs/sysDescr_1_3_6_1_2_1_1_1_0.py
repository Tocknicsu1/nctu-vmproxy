from pysnmp.proto import api
from snmp.mibs.base import Base


class sysDescr(Base):

    def resolve(self, protoVer):
        return api.protoModules[protoVer].OctetString(
            self.data['info']['instance_uuid']
        )
