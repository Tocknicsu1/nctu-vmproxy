import os
import fnmatch
import importlib
import inspect
import re

mibInstr = []


def include():
    ##################################################
    #        Include all files under dir             #
    ##################################################
    ignore = ['__init__.py', 'base.py', 'data_fetcher.py']
    for root, dirnames, filenames in os.walk('snmp/mibs'):
        for filename in fnmatch.filter(filenames, '*.py'):
            if filename in ignore:
                continue
            packagepath = 'snmp.mibs.%s' % (filename[:-3])
            package = importlib.import_module(packagepath)
            classes = inspect.getmembers(package, inspect.isclass)
            reg_exp = r'^.*%s\..*$' % (packagepath)
            for classname, classpath in classes:
                if re.match(reg_exp, str(classpath)):
                    mibInstr.append(getattr(package, classname)())


include()
mibInstr.sort()
