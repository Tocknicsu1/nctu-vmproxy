from pysnmp.proto import api
from snmp.mibs.base import Base


class hrStorageSize(Base):

    def resolve(self, protoVer):
        return api.protoModules[protoVer].Integer(
            self.data['performance']['memory']['total'] * 1024
        )
