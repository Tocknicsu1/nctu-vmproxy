from pysnmp.proto import api
from snmp.mibs.base import Base


class sysUpTime(Base):

    def resolve(self, protoVer):
        return api.protoModules[protoVer].TimeTicks(
            self.data['info']['uptime']
        )
