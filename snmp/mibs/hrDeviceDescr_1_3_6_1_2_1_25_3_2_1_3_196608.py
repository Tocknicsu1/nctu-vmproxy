from pysnmp.proto import api
from snmp.mibs.base import Base


class hrDeviceDescr(Base):

    def resolve(self, protoVer):
        return api.protoModules[protoVer].OctetString(
            "GenuineIntel: Intel(R) Core(TM) i3-2120 CPU @ 3.30GHz"
        )
