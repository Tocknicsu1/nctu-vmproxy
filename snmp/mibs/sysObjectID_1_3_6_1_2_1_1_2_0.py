from pysnmp.proto import api
from snmp.mibs.base import Base

# vmware https://pubs.vmware.com/vi3/sdk/ReferenceGuide/vim.vm.GuestOsDescriptor.GuestOsIdentifier.html
# snmp http://cric.grenoble.cnrs.fr/Administrateurs/Outils/MIBS/?oid=1.3.6.1.4.1.8072.3.2
# sysconfig guestId


class sysObjectID(Base):

    def resolve(self, protoVer):
        return api.protoModules[protoVer].ObjectIdentifier(
            (1, 3, 6, 1, 4, 1, 8072, 3, 2, 8)
        )
