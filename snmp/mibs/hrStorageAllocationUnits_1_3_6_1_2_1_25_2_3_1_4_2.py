from pysnmp.proto import api
from snmp.mibs.base import Base


class hrStorageAllocationUnits(Base):

    def resolve(self, protoVer):
        return api.protoModules[protoVer].Integer(1)
