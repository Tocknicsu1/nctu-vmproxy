from pysnmp.proto import api
from snmp.mibs.base import Base


class hrStorageType(Base):

    def resolve(self, protoVer):
        return api.protoModules[protoVer].ObjectIdentifier(
            (1, 3, 6, 1, 2, 1, 25, 3, 1, 21)
        )
