"""
This module is export Base for other mibs
"""
import inspect

from snmp.mibs.data_fetcher import fetcher


class Base:
    """
    mib base
    """

    def __init__(self):
        self.fetcher = fetcher
        self.data = None
        self._name = None

    def __eq__(self, other):
        return self.name == other

    def __ne__(self, other):
        return self.name != other

    def __lt__(self, other):
        return self.name < other

    def __le__(self, other):
        return self.name <= other

    def __gt__(self, other):
        return self.name > other

    def __ge__(self, other):
        return self.name >= other

    def __call__(self, proto_ver):
        self._fetch()
        return self.resolve(proto_ver)

    def _fetch(self):
        self.data = self.fetcher.fetch()

    def resolve(self, proto_ver):
        """
        need to implement by child
        """
        raise NotImplementedError

    @property
    def name(self):
        """
        oid
        """
        if not self._name:
            filename = inspect.getfile(self.__class__).split('/')[-1][:-3]
            self._name = tuple(map(int, filename.split('_')[1:]))
        return self._name


if __name__ == '__main__':
    base = Base()
    base(None)
