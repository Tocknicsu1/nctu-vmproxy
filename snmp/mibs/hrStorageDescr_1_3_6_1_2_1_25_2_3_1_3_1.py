from pysnmp.proto import api
from snmp.mibs.base import Base


class hrStorageDescr(Base):

    def resolve(self, protoVer):
        return api.protoModules[protoVer].OctetString(
            'Physical memory'
        )
