"""
    vm data fetcher
"""
import datetime

from snmp import config
from snmp.vm.vm_vm import Monitor


class DataFetcher():
    """
    vm data fetcher
    """

    def __init__(self):
        self.data = None
        self.last_update = None
        self.monitor = Monitor(
            host=config.HOST,
            user=config.USER,
            pwd=config.PASSWORD,
            uuid=config.VM_INSTANCE_UUID
        )
        self.fetch()

    def fetch(self):
        """
        fetch data with 60s cache
        """
        now = datetime.datetime.now()
        if self.last_update is None or (
                (now - self.last_update).total_seconds() > 60):
            self.data = self.monitor.fetch_data()
            self.last_update = datetime.datetime.now()
        return self.data


fetcher = DataFetcher()


if __name__ == '__main__':
    data = fetcher.fetch()
