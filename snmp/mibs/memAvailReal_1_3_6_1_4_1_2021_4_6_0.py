from pysnmp.proto import api
from snmp.mibs.base import Base


class memAvailReal(Base):

    def resolve(self, protoVer):
        return api.protoModules[protoVer].Integer(
            self.data['performance']['memory']['total'] * 1024 -
            self.data['performance']['memory']['usage'] * 1024
        )
