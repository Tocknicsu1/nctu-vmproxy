from snmp.vm.vm_host import Monitor as HostMonitor
from multiprocessing.dummy import Pool as ThreadPool 
import yaml
import sys
import config
import ipaddress
import urllib.parse
import requests
import json
import re
from queue import Queue

free_ip_address = Queue()

def fetch_vm_name(vm):
    now = vm
    full_name = now.name
    now = now.parent
    while now is not None:
        if now.name != 'vm':
            full_name = now.name + '/' + full_name
        now = now.parent
    return full_name

def fetch_vm_status(vm):
    return vm.summary.overallStatus

def gen_vm_status(vm):
    global free_ip_address
    data = gen_vm_status._data
    uuid = vm['summary.config.instanceUuid']
    print('process: %s' % (uuid))
    vm_data = {}
    if uuid not in data:
        vm_data['sync'] = False
        vm_data['librenms_id'] = 0
        vm_data['status'] = str(fetch_vm_status(vm['obj']))
        vm_data['path'] = fetch_vm_name(vm['obj'])
        vm_data['deprecated'] = False
        vm_data['instance_uuid'] = uuid
        vm_data['proxy_internal_ip_address'] = str(free_ip_address.get())
    else:
        vm_data = data[uuid]
        vm_data['status'] = str(fetch_vm_status(vm['obj']))
        vm_data['deprecated'] = False
    return vm_data

def gen_free_ip_address():
    def gen_list(ip_network_list):
        res = []
        for ip_network in ip_network_list:
            ip_network = ipaddress.ip_network(ip_network)
            res += list(ip_network.hosts()) + [ip_network.broadcast_address, ip_network.network_address]
        res = list(set(res))
        res.sort()
        return res

    included = gen_list(config.IP_ADDRESS)
    excluded = gen_list(config.IP_ADDRESS_RESERVED)

    used = [ ipaddress.ip_address(datum['proxy_internal_ip_address']) for datum in gen_vm_status._data.values() ]

    res = [ ip for ip in included if ip not in excluded and ip not in used]
    global free_ip_address
    for ip in res:
        free_ip_address.put(ip)

def gen_yaml():
    gen_vm_status._data = load_yaml()
    gen_free_ip_address()
    monitor = HostMonitor(
        host=config.HOST,
        user=config.USER,
        pwd=config.PASSWORD,
    )
    vm_list = monitor.fetch_data()
    print('fetch from vCenter')

    pool = ThreadPool(32) 

    data = pool.map(gen_vm_status, vm_list)

    data = {
        datum['instance_uuid']: datum for datum in data 
    }

    for datum in gen_vm_status._data:
        if datum not in data:
            data[datum] = gen_vm_status._data[datum]
            data[datum]['sync'] = False
            data[datum]['status'] = None
            data[datum]['deprecated'] = True
    
    print(data)

    with open('data.yaml', 'w', encoding='utf8') as outfile:
        yaml.dump(data, outfile, default_flow_style=False, allow_unicode=True)

def gen_dockerfile():
    data = load_yaml()
    docker = {}
    docker['version'] = '3'
    docker['networks'] = {
        'snmp-proxy-networks': {
            'driver': 'bridge',
            'ipam': {
                'config': [{
                    'subnet': '172.25.0.0/16'
                }]
            }
        }
    }
    docker['services'] = {}
    for datum in data.values():
        if not datum['sync']: continue
        tmp = {}
        tmp['build'] = {
            'context': './snmp'
        }
        tmp['image'] = 'snmp_proxy:latest'
        tmp['restart'] = 'always'
        tmp['environment'] = [
            'snmp_proxy_port=%s' % (161),
            'snmp_proxy_host=%s' % (config.HOST),
            'snmp_proxy_user=%s' % (config.USER),
            'snmp_proxy_password=%s' % (config.PASSWORD),
            'snmp_proxy_vm_instance_uuid=%s' % (datum['instance_uuid'])
        ]
        tmp['networks'] = {
            'snmp-proxy-networks': {
                'ipv4_address': datum['proxy_internal_ip_address']
            }
        }
        docker['services']['snmp_%s' % (datum['instance_uuid'])] = tmp

    with open('docker-compose.yaml', 'w', encoding='utf8') as outfile:
        yaml.dump(docker, outfile, default_flow_style=False, allow_unicode=True)
    print(docker)

def load_yaml():
    data = {}
    try:
        with open("data.yaml", 'r') as stream:
            data = yaml.load(stream)
    except:
        pass
    return data

def sync_with_librenms():
    def get_devices():
        url = urllib.parse.urljoin(config.LIBERNMS_WEB, 'api/v0/devices')
        content = json.loads(requests.get(url, headers={
            'X-Auth-Token': config.LIBRENMS_KEY
        }).text)
        if 'devices' not in content:
            return {}
        devices = content['devices']
        ret = {}
        for device in devices:
            ret[device['sysDescr']] = int(device['device_id'])
        return ret
    def add_device(data):
        url = urllib.parse.urljoin(config.LIBERNMS_WEB, 'api/v0/devices')
        content = json.loads(requests.post(url, json=data, headers={
            'X-Auth-Token': config.LIBRENMS_KEY
        }).text)
        return content


    devices = get_devices()
    data = load_yaml()
    for datum in data.values():
        if not datum['sync']: continue
        if datum['instance_uuid'] not in devices:
            result = add_device({
                'hostname': datum['proxy_internal_ip_address'],
                'force_add': True,
                'version': 'v2c',
                'community': 'public'
            })
            print(result)
            if result['status'] == 'ok':
                librenms_id = re.search('^Device.*\((\d+)\) has been added successfully$', result['message']).group(1)
                datum['librenms_id'] = int(librenms_id)
    with open('data.yaml', 'w', encoding='utf8') as outfile:
        yaml.dump(data, outfile, default_flow_style=False, allow_unicode=True)

def prompt():
    def get_input():
        command = -1
        while True:
            try:
                print("""
1) Sync with vCenter.
2) Generate Dockerfile.
3) Sync with librenms.
0) Exit.
Enter a number:""")
                command = int(input())
                return command
            except:
                print("You should enter a number.")
                pass
    command = get_input()
    if command == 0:
        sys.exit()
    elif command == 1:
        gen_yaml()
    elif command == 2:
        gen_dockerfile()
    elif command == 3:
        sync_with_librenms()
    else:
        pass


if __name__ == '__main__':
    while True:
        prompt()
